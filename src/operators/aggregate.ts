import { ICallMonad, IOperator, OutOf, InOf } from '../types'
import { Call } from '../..'

/**
 * aggregate creates a new operator from the passed in operators that simply combines the morphisms
 */
export function aggregate<T1, T2, T3> (op1: IOperator<ICallMonad<T1>, ICallMonad<T2>>, op2: IOperator<OutOf<typeof op1>, ICallMonad<T3>>): IOperator<InOf<typeof op1>, OutOf<typeof op2>>
export function aggregate<T1, T2, T3, T4> (op1: IOperator<ICallMonad<T1>, ICallMonad<T2>>, op2: IOperator<OutOf<typeof op1>, ICallMonad<T3>>, op3: IOperator<OutOf<typeof op2>, ICallMonad<T4>>): IOperator<InOf<typeof op1>, OutOf<typeof op3>>
export function aggregate<T1, T2, T3, T4, T5> (op1: IOperator<ICallMonad<T1>, ICallMonad<T2>>, op2: IOperator<OutOf<typeof op1>, ICallMonad<T3>>, op3: IOperator<OutOf<typeof op2>, ICallMonad<T4>>, op4: IOperator<OutOf<typeof op3>, ICallMonad<T4>>): IOperator<InOf<typeof op1>, OutOf<typeof op4>>
export function aggregate<T1, T2, T3, T4, T5, T6> (op1: IOperator<ICallMonad<T1>, ICallMonad<T2>>, op2: IOperator<OutOf<typeof op1>, ICallMonad<T3>>, op3: IOperator<OutOf<typeof op2>, ICallMonad<T4>>, op4: IOperator<OutOf<typeof op3>, ICallMonad<T4>>, op5: IOperator<OutOf<typeof op4>, ICallMonad<T5>>): IOperator<InOf<typeof op1>, OutOf<typeof op5>>
export function aggregate (...operators: IOperator[]): IOperator
export function aggregate (...operators: IOperator[]): IOperator {
  return operators.reduce((final, operator) => call => operator(final(call)))
  // return (instance: ICallMonad) => {
  //   const transform = operators.reduce((final, operator) => call => operator(final(call)))

  //   return Object.assign(transform(instance), { _fn: (x: any) => transform(Call.of(() => x))(x) })
  // }
}
