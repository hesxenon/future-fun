import { Call } from '../..'
import { ICallMonad, IOperator, UnaryFunction } from '../types'

export function flatMap<To, From, Root> (morphism: UnaryFunction<From, ICallMonad<To, any, From>>): IOperator<ICallMonad<From, Root>, ICallMonad<To, Root>> {
  return (instance: ICallMonad<From, Root>) => Call.of((x: Root) => {
    const result = instance(x)
    return morphism(result)(result)
  })
}
