export { flatMap } from './flatMap'
export { flatMapTo } from './flatMapTo'
export { map } from './map'
export { mapPromise } from './mapPromise'

export { aggregate } from './aggregate'
