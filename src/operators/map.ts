import { Call } from '../call'
import { ICallMonad, IOperator, UnaryFunction } from '../types'

export function map<From, To> (morphism: UnaryFunction<From, To>): IOperator<ICallMonad<From>, ICallMonad<To, From>> {
  return (instance: ICallMonad<From>) => Call.of((x: any) => morphism(instance(x)))
}
