import { IOperator, UnaryFunction, ICallMonad } from '../types'
import { Call } from '../..'

export function mapPromise<From, To> (morphism: UnaryFunction<From, To | Promise<To>>): IOperator<ICallMonad<Promise<From>>, ICallMonad<Promise<To>>> {
  return (instance: ICallMonad<Promise<From>>) => Call.of((x: any) => instance(x).then(morphism))
}
