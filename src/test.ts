import { Call } from '..'
import { ICallMonad, IOperator } from './types'

export function testCall<In, Out> (call: ICallMonad<Out, In, any>, arg: In) {
  return call._fn(arg)
}

export function testOperator<To, From> (op: IOperator<ICallMonad<From>, ICallMonad<To, From>>, arg: From): To {
  return op(Call.identity)(arg)
}
