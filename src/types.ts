export interface ICallMonad<Out = any, In = any, Root = In> {
  (arg: Root): Out
  pipe: IPipe
  _fn: (arg: In) => Out
}

export interface IPipedCallMonad<Out, Previous extends ICallMonad> extends ICallMonad<Out, OutOf<Previous>, InOf<Previous>> {
  previous: Previous
}

export interface IAllCallMonad<Calls extends ICallMonad[]> extends ICallMonad<OutOfAll<Calls>, InOfAll<Calls>> {
  calls: Calls
}

export interface ILift {
  <Out> (fn: NullaryFunction<Out>, thisArg?: any): ICallMonad<Out>
  <In, Out> (fn: UnaryFunction<In, Out>, thisArg?: any): ICallMonad<Out, In>
}

export interface IAll {
  <M1 extends ICallMonad, M2 extends ICallMonad> (m1: M1, m2: M2): IAllCallMonad<[M1, M2]>
  <M1 extends ICallMonad, M2 extends ICallMonad, M3 extends ICallMonad> (m1: M1, m2: M2, m3: M3): IAllCallMonad<[M1, M2, M3]>
  <M1 extends ICallMonad, M2 extends ICallMonad, M3 extends ICallMonad, M4 extends ICallMonad> (m1: M1, m2: M2, m3: M3, m4: M4): IAllCallMonad<[M1, M2, M3, M4]>
  <M1 extends ICallMonad, M2 extends ICallMonad, M3 extends ICallMonad, M4 extends ICallMonad, M5 extends ICallMonad> (m1: M1, m2: M2, m3: M3, m4: M4, m5: M5): IAllCallMonad<[M1, M2, M3, M4, M5]>
  (...calls: ICallMonad[]): IAllCallMonad<typeof calls>
}

export type IOperator<In extends ICallMonad = ICallMonad, Out extends ICallMonad<any, any, InOf<In>> = ICallMonad> = UnaryFunction<In, Out>

export interface IAggregate {
  <T1, T2> (op1: IOperator<ICallMonad, ICallMonad<T1>>, op2: IOperator<OutOf<typeof op1>, ICallMonad<T2, T1>>): IOperator<InOf<typeof op1>, OutOf<typeof op2>>
  <T1, T2, T3> (op1: IOperator<ICallMonad, ICallMonad<T1>>, op2: IOperator<OutOf<typeof op1>, ICallMonad<T2, T1>>, op3: IOperator<OutOf<typeof op2>, ICallMonad<T3, T1>>): IOperator<InOf<typeof op1>, OutOf<typeof op3>>
  <T1, T2, T3, T4> (op1: IOperator<ICallMonad, ICallMonad<T1>>, op2: IOperator<OutOf<typeof op1>, ICallMonad<T2, T1>>, op3: IOperator<OutOf<typeof op2>, ICallMonad<T3, T1>>, op4: IOperator<OutOf<typeof op3>, ICallMonad<T4, T1>>): IOperator<InOf<typeof op1>, OutOf<typeof op4>>
  <T1, T2, T3, T4, T5> (op1: IOperator<ICallMonad, ICallMonad<T1>>, op2: IOperator<OutOf<typeof op1>, ICallMonad<T2, T1>>, op3: IOperator<OutOf<typeof op2>, ICallMonad<T3, T1>>, op4: IOperator<OutOf<typeof op3>, ICallMonad<T4, T1>>, op5: IOperator<OutOf<typeof op4>, ICallMonad<T5, T1>>): IOperator<InOf<typeof op1>, OutOf<typeof op5>>
  (...operators: IOperator<any, any>[]): IOperator<any, any>
}

export interface IPipe {
  <I extends ICallMonad, O1> (this: I, op1: IOperator<ICallMonad<OutOf<I>>, ICallMonad<O1>>): IPipedCallMonad<O1, I>
  <I extends ICallMonad, O1, O2> (this: I, op1: IOperator<ICallMonad<OutOf<I>>, ICallMonad<O1>>, op2: IOperator<OutOf<typeof op1>, ICallMonad<O2>>): IPipedCallMonad<O2, I>
  <I extends ICallMonad, O1, O2, O3> (this: I, op1: IOperator<ICallMonad<OutOf<I>>, ICallMonad<O1>>, op2: IOperator<OutOf<typeof op1>, ICallMonad<O2>>, op3: IOperator<OutOf<typeof op2>, ICallMonad<O3>>): IPipedCallMonad<O3, I>
  <I extends ICallMonad, O1, O2, O3, O4> (this: I, op1: IOperator<ICallMonad<OutOf<I>>, ICallMonad<O1>>, op2: IOperator<OutOf<typeof op1>, ICallMonad<O2>>, op3: IOperator<OutOf<typeof op2>, ICallMonad<O3>>, op4: IOperator<OutOf<typeof op3>, ICallMonad<O4>>): IPipedCallMonad<O4, I>
  <I extends ICallMonad, O1, O2, O3, O4, O5> (this: I, op1: IOperator<ICallMonad<OutOf<I>>, ICallMonad<O1>>, op2: IOperator<OutOf<typeof op1>, ICallMonad<O2>>, op3: IOperator<OutOf<typeof op2>, ICallMonad<O3>>, op4: IOperator<OutOf<typeof op3>, ICallMonad<O4>>, op5: IOperator<OutOf<typeof op4>, ICallMonad<O5>>): IPipedCallMonad<O5, I>
}

export type UnaryFunction<In, Out> = (arg: In) => Out
export type NullaryFunction<Out> = () => Out

export type Morphism = UnaryFunction<any, any> | NullaryFunction<any>

export type InOf<T> =
  T extends ICallMonad<any, any, infer In> ? In :
  T extends IOperator<infer In, any> ? In :
  T extends UnaryFunction<infer In, any> ? In :
  T extends NullaryFunction<any> ? undefined :
  any

export type OutOf<T> =
  T extends ICallMonad<infer Out> ? Out :
  T extends IOperator<any, infer Out> ? Out :
  T extends UnaryFunction<any, infer Out> ? Out :
  T extends NullaryFunction<infer Out> ? Out :
  any

export type InOfAll<T> =
  T extends [ICallMonad<any, any, infer I1>, ICallMonad<any, any, infer I2>] ? [I1, I2] :
  T extends [ICallMonad<any, any, infer I1>, ICallMonad<any, any, infer I2>, ICallMonad<any, any, infer I3>] ? [I1, I2, I3] :
  T extends [ICallMonad<any, any, infer I1>, ICallMonad<any, any, infer I2>, ICallMonad<any, any, infer I3>, ICallMonad<any, any, infer I4>] ? [I1, I2, I3, I4] :
  T extends [ICallMonad<any, any, infer I1>, ICallMonad<any, any, infer I2>, ICallMonad<any, any, infer I3>, ICallMonad<any, any, infer I4>, ICallMonad<any, any, infer I5>] ? [I1, I2, I3, I4, I5] :
  any[]

export type OutOfAll<T> =
  T extends [ICallMonad<infer O1>, ICallMonad<infer O2>] ? [O1, O2] :
  T extends [ICallMonad<infer O1>, ICallMonad<infer O2>, ICallMonad<infer O3>] ? [O1, O2, O3] :
  T extends [ICallMonad<infer O1>, ICallMonad<infer O2>, ICallMonad<infer O3>, ICallMonad<infer O4>] ? [O1, O2, O3, O4] :
  T extends [ICallMonad<infer O1>, ICallMonad<infer O2>, ICallMonad<infer O3>, ICallMonad<infer O4>, ICallMonad<infer O5>] ? [O1, O2, O3, O4, O5] :
  any[]
