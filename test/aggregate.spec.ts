import { assert } from 'chai'
import { aggregate, Call, map } from '..'
import { testCall } from '../src/test'
import { double, ident } from './test.util'

describe('aggregate', () => {
  it('should make it possible to compose multiple operators into one', () => {
    const a = Call.of(ident)
    const quadruple = aggregate(
      map(double),
      map(double)
    )
    assert(a.pipe(quadruple)(1) === 4)
  })

  it('should be possible to aggregate through multiple types', () => {
    const stringifyAndParse = aggregate(
      map((x: number) => {
        return x + ''
      }), map(x => {
        return parseInt(x, 10)
      })
    )
    const c = Call.of(double).pipe(stringifyAndParse)
    assert(c(1) === 2)
    assert(testCall(c, 1) === 1)
  })
})
